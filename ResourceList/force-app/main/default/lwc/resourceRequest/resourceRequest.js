import { LightningElement, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { createRecord } from 'lightning/uiRecordApi';
import { updateRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import ACCOUNT_OBJECT from '@salesforce/schema/Account';
import RESOURCE_OBJECT from '@salesforce/schema/Social_Services_Assesment__c'
import checkAccounts from '@salesforce/apex/resourceAccount.checkAccounts';
import adobeLogo from '@salesforce/resourceUrl/Adobe_Logo_2';
import coyote from '@salesforce/resourceUrl/Coyote';
import sunset from '@salesforce/resourceUrl/Sunset3';
import maslow from '@salesforce/resourceUrl/Maslow';
import healthcare from '@salesforce/resourceUrl/Healthcare';
import newlogo from '@salesforce/resourceUrl/NewLogoSmall';
import successstory from '@salesforce/resourceUrl/SuccessStory';
import maslowLogo from '@salesforce/contentAssetUrl/Maslow_color_mark';
export default class ResourceRequest extends NavigationMixin(LightningElement) {

    adobeLogoUrl = `${adobeLogo}`;
    coyoteUrl = `${coyote}`;
    sunsetUrl = `${sunset}`;
    maslowUrl = `${maslow}`;
    healthcareUrl = `${healthcare}`;
    newlogoUrl = `${newlogo}`;
    successstoryUrl = `${successstory}`;
    maslowLogoUrl = maslowLogo;

    page1 = true;
    page2 = false;
    page3 = false;
    page4 = false;

    language = true;
    instate = false;
    created = false;
    loading = false;
    today = new Date()
    maxDate = String(this.today.getFullYear()) + '-' + String(this.today.getMonth() + 1).padStart(2, '0') + '-' + String(this.today.getDate() - 1).padStart(2, '0');

    get genders() {
        return [
            { label: 'Male', value: 'Male' },
            { label: 'Female', value: 'Female' },
            { label: 'Other', value: 'Other' }
        ];
    }

    get county() {
        return [
            { label: 'Apache', value: 'Apache' },
            { label: 'Cochise', value: 'Cochise' },
            { label: 'Coconino', value: 'Coconino' },
            { label: 'Gila', value: 'Gila' },
            { label: 'Graham', value: 'Graham' },
            { label: 'Greenlee', value: 'Greenlee' },
            { label: 'La Paz', value: 'La Paz' },
            { label: 'Maricopa', value: 'Maricopa' },
            { label: 'Mohave', value: 'Mohave' },
            { label: 'Navajo', value: 'Navajo' },
            { label: 'Pima', value: 'Pima' },
            { label: 'Pinal', value: 'Pinal' },
            { label: 'Santa Cruz', value: 'Santa Cruz' },
            { label: 'Yavapai', value: 'Yavapai' },
            { label: 'Yuma', value: 'Yuma' },
            { label: 'Out of State', value: 'Out of State' },
        ]
    }

    get states() {
        return[
            { label: 'Alabama', value: 'Alabama' },
            { label: 'Alaska', value: 'Alaska' },
            { label: 'Arizona', value: 'Arizona' },
            { label: 'Arkansas', value: 'Arkansas' },
            { label: 'California', value: 'California' },
            { label: 'Colorado', value: 'Colorado' },
            { label: 'Connecticut', value: 'Connecticut' },
            { label: 'Delaware', value: 'Delaware' },
            { label: 'Florida', value: 'Florida' },
            { label: 'Georgia', value: 'Georgia' },
            { label: 'Hawaii', value: 'Hawaii' },
            { label: 'Idaho', value: 'Idaho' },
            { label: 'Illinois', value: 'Illinois' },
            { label: 'Indiana', value: 'Indiana' },
            { label: 'Iowa', value: 'Iowa' },
            { label: 'Kansas', value: 'Kansas' },
            { label: 'Kentucky', value: 'Kentucky' },
            { label: 'Louisiana', value: 'Louisiana' },
            { label: 'Maine', value: 'Maine' },
            { label: 'Maryland', value: 'Maryland' },
            { label: 'Massachusetts', value: 'Massachusetts' },
            { label: 'Michigan', value: 'Michigan' },
            { label: 'Minnesota', value: 'Minnesota' },
            { label: 'Mississippi', value: 'Mississippi' },
            { label: 'Missouri', value: 'Missouri' },
            { label: 'Montana', value: 'Montana' },
            { label: 'Nebraska', value: 'Nebraska' },
            { label: 'Nevada', value: 'Nevada' },
            { label: 'New Hampshire', value: 'New Hampshire' },
            { label: 'New Jersey', value: 'New Jersey' },
            { label: 'New Mexico', value: 'New Mexico' },
            { label: 'New York', value: 'New York' },
            { label: 'North Carolina', value: 'North Carolina' },
            { label: 'North Dakota', value: 'North Dakota' },
            { label: 'Ohio', value: 'Ohio' },
            { label: 'Oklahoma', value: 'Oklahoma' },
            { label: 'Oregon', value: 'Oregon' },
            { label: 'Pennsylvania', value: 'Pennsylvania' },
            { label: 'Rhode Island', value: 'Rhode Island' },
            { label: 'South Carolina', value: 'South Carolina' },
            { label: 'South Dakota', value: 'South Dakota' },
            { label: 'Tennessee', value: 'Tennessee' },
            { label: 'Texas', value: 'Texas' },
            { label: 'Utah', value: 'Utah' },
            { label: 'Vermont', value: 'Vermont' },
            { label: 'Virginia', value: 'Virginia' },
            { label: 'Washington', value: 'Washington' },
            { label: 'West Virginia', value: 'West Virginia' },
            { label: 'Wisconsin', value: 'Wisconsin' },
            { label: 'Wyoming', value: 'Wyoming' }
        ]
    }

    get residences() {
        return [
            { label: 'Rental apartment', value: 'Rental apartment' },
            { label: 'House or condominium', value: 'House or condominium' },
            { label: 'Homeless', value: 'Homeless' },
            { label: 'Shelter', value: 'Shelter' },
        ]
    }

    get yesorno() {
        return [
            { label: 'Yes', value: 'Yes' },
            { label: 'No', value: 'No' },
        ]
    }

    get transportation() {
        return [
            { label: 'Drive', value: 'Drive' },
            { label: 'Family', value: 'Family' },
            { label: 'Public transportation', value: 'Public transportation' },
            { label: 'No fixed arrangement', value: 'No fixed arrangement' },
        ]
    }

    @track recordInput = {
        apiName: ACCOUNT_OBJECT.objectApiName,
        fields: {
            RecordTypeId: '012360000004FEPAA2',
            Name: '',
            First_Name__c: '',
            Surname__c: '',
            Phone: '',
            Gender__c: '',
            Birthdate__c: '',
            BillingStreet: '',
            BillingCity: '',
            BillingState: '',
            County__c: '',
            BillingPostalCode: '',
            Primary_Insurance__c: 'Health Net',
            Status__c: 'Active',
        }
    };

    @track updateInput = {
        fields: {
            Id: '',
            Phone: '',
            Gender__c: '',
            BillingStreet: '',
            BillingCity: '',
            BillingState: '',
            County__c: '',
            BillingPostalCode: '',
            Status__c: 'Active',
        }
    };

    @track resourceInput = {
        apiName: RESOURCE_OBJECT.objectApiName,
        fields: {
            RecordTypeId: '0121R000001DvrfQAC',
            Account__c: '',
            Radius_in_miles__c: '25', 
            What_type_of_residence_do_you_live_in__c: '',
            Are_you_having_financial_difficulties__c: '',
            Can_you_pay_for_utilities__c: '',
            Can_you_pay_for_housing__c: '',
            Can_you_pay_for_your_medical_visits__c: '',
            Can_you_pay_for_your_transportation__c: '',
            Do_you_eat_at_least_two_meals_a_day__c: '',
            Can_you_pay_for_food__c: '',
            Can_you_pay_for_your_medication__c: '',
            How_do_you_get_to_medical_appointments__c: '',
            Do_you_feel_safe_at_home__c: '',
            Police_called_to_home_in_last_90_days__c: '',
            Social_Worker_requested__c: '',
        }
    };

    @track resourceId = '';

    handleInputChange(event) {
        if (event.target.name == "FirstName") {
            /*if (event.target.value.indexOf(' ') >= 0) {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Please remove blank spaces from "First Name".',
                        variant: "error"
                    }),
                );
            }
            if (!event.target.value.match(/[a-z]/i) && event.target.value.length > 0) {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Only letters are allowed in "First Name".',
                        variant: "error"
                    }),
                );
            }
            else {*/
            this.recordInput.fields.First_Name__c = event.target.value;
            this.created = false;
            //}
        }
        else if (event.target.name == "LastName") {
            this.recordInput.fields.Surname__c = event.target.value;
            this.created = false;
        }
        else if (event.target.name == "Phone") {
            if (event.target.value.match(/\D/i)) {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Please only use numbers',
                        variant: "error"
                    }),
                );
            }
            else {
                this.recordInput.fields.Phone = event.target.value;
                this.updateInput.fields.Phone = event.target.value;
                this.created = false;
            }
        }
        else if (event.target.name == "Gender") {
            this.recordInput.fields.Gender__c = event.target.value;
            this.updateInput.fields.Gender__c = event.target.value;
            this.created = false;
        }
        else if (event.target.name == "Birthdate") {
            this.recordInput.fields.Birthdate__c = event.target.value;
            this.created = false;
        }
        else if (event.target.name == "Address") {
            this.recordInput.fields.BillingStreet = event.target.value;
            this.updateInput.fields.BillingStreet = event.target.value;
            this.recordInput.fields.Name = this.recordInput.fields.First_Name__c + " " + this.recordInput.fields.Surname__c;
            this.created = false;
        }
        else if (event.target.name == "City") {
            this.recordInput.fields.BillingCity = event.target.value;
            this.updateInput.fields.BillingCity = event.target.value;
            this.created = false;
        }
        else if (event.target.name == "State") {
            this.recordInput.fields.BillingState = event.target.value;
            this.updateInput.fields.BillingState = event.target.value;
            this.created = false;
            if (event.target.value == "Arizona")
                this.instate = true;
            else {
                this.instate = false;
                this.recordInput.fields.County__c = "Out of State";
                this.updateInput.fields.County__c = "Out of State";
            }
        }
        else if (event.target.name == "County") {
            this.recordInput.fields.County__c = event.target.value;
            this.updateInput.fields.County__c = event.target.value;
            this.created = false;
        }
        else if (event.target.name == "Zip") {
            this.recordInput.fields.BillingPostalCode = event.target.value;
            this.updateInput.fields.BillingPostalCode = event.target.value;
            this.created = false;
        }
        else if (event.target.name == "One")
            this.resourceInput.fields.What_type_of_residence_do_you_live_in__c = event.target.value;
        else if (event.target.name == "Two")
            this.resourceInput.fields.Are_you_having_financial_difficulties__c = event.target.value;
        else if (event.target.name == "Three")
            this.resourceInput.fields.Can_you_pay_for_utilities__c = event.target.value;
        else if (event.target.name == "Four")
            this.resourceInput.fields.Can_you_pay_for_housing__c = event.target.value;
        else if (event.target.name == "Five")
            this.resourceInput.fields.Can_you_pay_for_your_medical_visits__c = event.target.value;
        else if (event.target.name == "Six")
            this.resourceInput.fields.Can_you_pay_for_your_transportation__c = event.target.value;
        else if (event.target.name == "Seven")
            this.resourceInput.fields.Do_you_eat_at_least_two_meals_a_day__c = event.target.value;
        else if (event.target.name == "Eight")
            this.resourceInput.fields.Can_you_pay_for_food__c = event.target.value;
        else if (event.target.name == "Nine")
            this.resourceInput.fields.Can_you_pay_for_your_medication__c = event.target.value;
        else if (event.target.name == "Ten")
            this.resourceInput.fields.How_do_you_get_to_medical_appointments__c = event.target.value;
        else if (event.target.name == "Eleven")
            this.resourceInput.fields.Do_you_feel_safe_at_home__c = event.target.value;
        else if (event.target.name == "Twelve")
            this.resourceInput.fields.Police_called_to_home_in_last_90_days__c = event.target.value;
        else if (event.target.name == "Thirteen")
            this.resourceInput.fields.Social_Worker_requested__c = event.target.value;
    }

    handleClick(event) {
        const toastEvent = new ShowToastEvent({
            title: "Please fill out all questions.",
            variant : "error"
        });
        const spanishtoastEvent = new ShowToastEvent({
            title: "Por favor complete todas las preguntas.",
            variant : "error"
        });
        const that = this;
        if (event.target.name == "english") {
            this.language = true;
            this.page1 = false;
            this.page2 = true;
        }
        else if (event.target.name == "spanish") {
            this.language = false;
            this.page1 = false;
            this.page2 = true;
        }
        else if (event.target.name == "back1") {
            this.page2 = false;
            this.page1 = true;
        }
        else if (event.target.name == "next1") {
            if (this.recordInput.fields.First_Name__c != "" && this.recordInput.fields.Surname__c != "" && this.recordInput.fields.Phone != "" && this.recordInput.fields.Gender__c != "" && this.recordInput.fields.Birthdate__c != "") {
                checkAccounts({ fn: this.recordInput.fields.First_Name__c, ln: this.recordInput.fields.Surname__c, bd: this.recordInput.fields.Birthdate__c })
                    .then(result => {
                        this.updateInput.fields.Id = result.Id;
                        this.resourceInput.fields.Account__c = result.Id;
                        /*this.dispatchEvent(
                            new ShowToastEvent({
                                title: "Account Exists",
                                message: result.Id,
                                variant: "success"
                            }),
                        );*/
                    })
                    .catch(error => {
                        this.updateInput.fields.Id = '';
                        /*this.dispatchEvent(
                            new ShowToastEvent({
                                title: "Account Does Not Exist",
                                message: error,
                                variant: "error"
                            }),
                        );*/
                    });
                    this.page2 = false;
                    this.page3 = true;
            }
            else
                if (this.language)
                    this.dispatchEvent(toastEvent);
                else
                    this.dispatchEvent(spanishtoastEvent);
        }
        else if (event.target.name == "back2") {
            this.page2 = true;
            this.page3 = false;
        }
        else if (event.target.name == "next2") {
            if (this.recordInput.fields.BillingStreet != "" && this.recordInput.fields.BillingCity != "" && this.recordInput.fields.BillingState != "" && this.recordInput.fields.BillingPostalCode != "" && this.recordInput.fields.County__c != "") {
                if (this.updateInput.fields.Id == '' && this.created == false) {
                    createRecord(this.recordInput)
                    .then(person => {
                        this.resourceInput.fields.Account__c = person.id;
                        this.updateInput.fields.Id = person.id;
                        this.created = true;
                    })
                    .catch(error => {
                        this.error = error;
                        this.dispatchEvent(
                            new ShowToastEvent({
                                title: "Error",
                                message: error.body.message,
                                variant: "error"
                            }),
                        );
                    });
                }
                else {
                    updateRecord(this.updateInput)
                    .catch(error => {
                        this.error = error;
                        this.dispatchEvent(
                            new ShowToastEvent({
                                title: "Error",
                                message: error.body.message,
                                variant: "error"
                            }),
                        );
                    });
                }
                this.page3 = false;
                this.page4 = true;
            }
            else 
                if (this.language)
                    this.dispatchEvent(toastEvent);
                else
                    this.dispatchEvent(spanishtoastEvent);
        }
        else if (event.target.name == "back3") {
            this.page3 = true;
            this.page4 = false;
        }
        else if (event.target.name == "next3") {
            if (this.resourceInput.fields.What_type_of_residence_do_you_live_in__c != "" && this.resourceInput.fields.Are_you_having_financial_difficulties__c != "" && this.resourceInput.fields.Can_you_pay_for_utilities__c != "" && this.resourceInput.fields.Can_you_pay_for_your_housing__c != "" && this.resourceInput.fields.Can_you_pay_for_your_medical_visits__c != "" && this.resourceInput.fields.Can_you_pay_for_your_transportation__c != "" && this.resourceInput.fields.Do_you_eat_at_least_two_meals_a_day__c != "" && this.resourceInput.fields.Can_you_pay_for_food__c != "" && this.resourceInput.fields.Can_you_pay_for_your_medication__c != "" && this.resourceInput.fields.How_do_you_get_to_medical_appointments__c != "" && this.resourceInput.fields.Do_you_feel_safe_at_home__c != "" && this.resourceInput.fields.Police_called_to_home_in_last_90_days__c != "" && this.resourceInput.fields.Social_Worker_requested__c != "") {
                this.loading = true;
                createRecord(this.resourceInput)
                .then(resource => {
                    that.resourceId = resource.id;
                    /*this.dispatchEvent(
                        new ShowToastEvent({
                            title: "Record Created",
                            message: this.resourceId,
                            variant: "success"
                        }),
                    );*/
                    this.ready = true;
                    setTimeout(
                        this[NavigationMixin.Navigate]({
                            type: 'standard__recordPage',
                            attributes: {
                                recordId: this.resourceId,
                                objectApiName: RESOURCE_OBJECT.objectApiName,
                                actionName: 'view'
                            }
                        }), 2000
                    );
                })
                .catch(error => {
                    this.error = error;
                });
            }
            else 
                if (this.language)
                    this.dispatchEvent(toastEvent);
                else
                    this.dispatchEvent(spanishtoastEvent);
        }
    }
}