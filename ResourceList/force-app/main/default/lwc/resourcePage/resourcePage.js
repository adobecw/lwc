import { LightningElement, wire, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { createRecord, updateRecord } from 'lightning/uiRecordApi';
import SELECTED_RESOURCE_OBJECT from '@salesforce/schema/Selected_Resource__c'
import searchResources from '@salesforce/apex/resourceList.searchResources';
import searchRecord from '@salesforce/apex/resourceList.searchRecord';
import sendResources from '@salesforce/apex/resourceList.sendResources';
import searchActiveResources from '@salesforce/apex/resourceList.searchActiveResources';

export default class ResourcePage extends NavigationMixin(LightningElement) {

	@api recordId;
	@track record;
	@track state;
	shown = false;
	latitude;
	longitude;
	What_type_of_residence_do_you_live_in__c;
	Are_you_having_financial_difficulties__c;
	Can_you_pay_for_utilities__c;
	Can_you_pay_for_housing__c;
	Can_you_pay_for_your_medical_visits__c;
	Can_you_pay_for_your_transportation__c;
	Do_you_eat_at_least_two_meals_a_day__c;
	Can_you_pay_for_food__c;
	Can_you_pay_for_your_medication__c;
	How_do_you_get_to_medical_appointments__c;
	Do_you_feel_safe_at_home__c;
	Police_called_to_home_in_last_90_days__c;
	@track initialResources = [];
	length;
	@track filteredResources;
	@track results;
	@track error;
	@track noResources = false;
	@track listLoading = true;
	@track noSelectedResources = true;
	@track type = 'Recommended Resources';
	@track distance = '25';
	@track selectedResources = new Array();
	@track recordInput = {
		apiName: SELECTED_RESOURCE_OBJECT.objectApiName,
		fields: {
			Name__c: '',
			Description__c: '',
			Phone__c: '',
			Street_Address__c: '',
			City__c: '',
			State__c: '',
			Zip__c: '',
			Food_Bank__c: false,
			Financial_Assistance__c: false,
			Shelter__c: false,
			Case_Management__c: false,
			Serves_Meals__c: false,
			Senior_Center__c: false,
			Housing_Assistance__c	: false,
			Transportation__c: false,
			Social_Services_Assessment__c: '',
			Account__c: '',
			Resource_Account__c: '',
		}
	};
	@track updateInput = {
		fields: {
            Id: '',
            Email__c: '',
        }
	};
	@track mapMarkers;
	selectedMarkerValue;
	center;
	@track isModalOpen = false;
	@track isHelpModalOpen = false;
	@track screenLoading = false;
	@track vfpage = false;
	@track activeResources = new Array();

	get types() {
		return [
			{ label: 'Recommended Resources', value: 'Recommended Resources' },
			{ label: 'All', value: 'All' },
			{ label: 'Food Bank', value: 'Food Bank' },
			{ label: 'Shelter', value: 'Shelter' },
			{ label: 'Serves Meals', value: 'Serves Meals' },
			{ label: 'Housing Assistance', value: 'Housing Assistance' },
			{ label: 'Financial Assistance', value: 'Financial Assistance' },
			{ label: 'Case Management', value: 'Case Management' },
			{ label: 'Senior Center', value: 'Senior Center' },
			{ label: 'Transportation', value: 'Transportation' }
		]
	}

	get distances() {
		return [
            { label: '5 Miles', value: '5' },
			{ label: '10 Miles', value: '10' },            
			{ label: '15 Miles', value: '15' },
			{ label: '20 Miles', value: '20' },
			{ label: '25 Miles', value: '25' },
			{ label: '30 Miles', value: '30' },
			{ label: '40 Miles', value: '40' },
			{ label: '50 Miles', value: '50' },
			{ label: 'Whole State', value: '1000'}
        ];
	}

	@wire(searchRecord, { id: '$recordId' })
	loadRecord({data, error}) {
		if (data) {
			this.record = data;
			this.state = this.record[0].Account__r.BillingState;
			this.center = {
				location: {
					Street: this.record[0].Account__r.BillingStreet,
					City: this.record[0].Account__r.BillingCity,
					State: this.record[0].Account__r.BillingState
				}
			};
			this.latitude = this.record[0].Account__r.Latitude__c;
			this.longitude = this.record[0].Account__r.Longitude__c;
			this.What_type_of_residence_do_you_live_in__c = this.record[0].What_type_of_residence_do_you_live_in__c;
			this.Are_you_having_financial_difficulties__c = this.record[0].Are_you_having_financial_difficulties__c;
			this.Can_you_pay_for_utilities__c = this.record[0].Can_you_pay_for_utilities__c;
			this.Can_you_pay_for_housing__c = this.record[0].Can_you_pay_for_housing__c;
			this.Can_you_pay_for_your_medical_visits__c = this.record[0].Can_you_pay_for_your_medical_visits__c;
			this.Can_you_pay_for_your_transportation__c = this.record[0].Can_you_pay_for_your_transportation__c;
			this.Do_you_eat_at_least_two_meals_a_day__c = this.record[0].Do_you_eat_at_least_two_meals_a_day__c;
			this.Can_you_pay_for_food__c = this.record[0].Can_you_pay_for_food__c;
			this.Can_you_pay_for_your_medication__c = this.record[0].Can_you_pay_for_your_medication__c;
			this.How_do_you_get_to_medical_appointments__c = this.record[0].How_do_you_get_to_medical_appointments__c;
			this.Do_you_feel_safe_at_home__c = this.record[0].Do_you_feel_safe_at_home__c;
			this.Police_called_to_home_in_last_90_days__c = this.record[0].Police_called_to_home_in_last_90_days__c;
			this.updateInput.fields.Id = this.record[0].Account__r.Id;
			if (!this.shown) {
				setTimeout(
					this.dispatchEvent(
						new ShowToastEvent({
							title: "Welcome to the Resource Selection Page",
							message: 'Click the "Help" button in the top right for more information.',
							variant: "info",
							mode: "sticky"
						}),
					), 3000
				);
				this.shown = true;
			}
		}
		else
			this.record = error;
	}

	@wire(searchResources, { state: '$state', type: '$type', distance: '$distance', latitude: '$latitude', longitude: '$longitude', What_type_of_residence_do_you_live_in: '$What_type_of_residence_do_you_live_in__c', Are_you_having_financial_difficulties: '$Are_you_having_financial_difficulties__c', Can_you_pay_for_utilities: '$Can_you_pay_for_utilities__c', Can_you_pay_for_housing: '$Can_you_pay_for_housing__c', Can_you_pay_for_your_medical_visits: '$Can_you_pay_for_your_medical_visits__c', Can_you_pay_for_your_transportation: '$Can_you_pay_for_your_transportation__c', Do_you_eat_at_least_two_meals_a_day: '$Do_you_eat_at_least_two_meals_a_day__c', Can_you_pay_for_food: '$Can_you_pay_for_food__c', Can_you_pay_for_your_medication: '$Can_you_pay_for_your_medication__c', How_do_you_get_to_medical_appointments: '$How_do_you_get_to_medical_appointments__c', Do_you_feel_safe_at_home: '$Do_you_feel_safe_at_home__c', Police_called_to_home_in_last_90_days: '$Police_called_to_home_in_last_90_days__c'})
	loadResources(result) {
		if (result.data) {
			this.results = result.data;
			this.initialResources = [];
			this.length = 0;
			for (let key in result.data) {
				this.initialResources.push({value: result.data[key], key: Number(key).toFixed(1), phone: '(' + String(result.data[key].Phone).substring((String(result.data[key].Phone).length - 10), (String(result.data[key].Phone).length - 7)) + ')-' + String(result.data[key].Phone).substring((String(result.data[key].Phone).length - 7), (String(result.data[key].Phone).length - 4)) + '-' + String(result.data[key].Phone).substring((String(result.data[key].Phone).length - 4), String(result.data[key].Phone).length)});
				this.length++;
			}
			this.listLoading = false;
			if (this.length === 0) {
				this.noResources = true;
				this.mapMarkers = [];
			}
			else
				this.noResources = false;
				this.filteredResources = this.initialResources
				if (this.length > 10)
					this.length = 10;
				let tempMarkers = [];
				for (var i = 0; i < this.length; i++) {
					let marker = {
						location: {
							Latitude: this.initialResources[i].value.Latitude__c,
        					Longitude: this.initialResources[i].value.Longitude__c
						},
						title: this.initialResources[i].value.Name,
						value: this.initialResources[i].value.Id,
						description: this.initialResources[i].value.Description + '<br>' + this.initialResources[i].key + ' mi.',
					};
					tempMarkers.push(marker);
				}
				this.mapMarkers = tempMarkers;
		}
		else if (result.error) {
			this.error = result.error;
			this.dispatchEvent(
				new ShowToastEvent({
					title: 'Error',
					message: this.error.body.message,
					variant: "error"
				}),
			);
		} 
		else
			this.dispatchEvent(
				new ShowToastEvent({
					title: 'Error',
					message: 'No Idea',
					variant: "error"
				}),
			);
	}
	
	handleShowMarker(event) {
		this.selectedMarkerValue = event.detail;
	}

    handleAddResource(event) {
		let resourceId = event.detail;
		let tempIds = [];
		for (let i = 0; i < this.selectedResources.length; i++) {
			tempIds.push(this.selectedResources[i].value.Id);
		}
		for (let i = 0; i < this.filteredResources.length; i++) {
			if (resourceId == this.filteredResources[i].value.Id && !tempIds.includes(this.filteredResources[i].value.Id)) {
				this.selectedResources.push(this.filteredResources[i]);
			}
		}
		this.noSelectedResources = false;
	}

	handleAddAll() {
		let tempIds = [];
		for (let i = 0; i < this.selectedResources.length; i++) {
			tempIds.push(this.selectedResources[i].value.Id);
		}
		for (let i = 0; i < this.filteredResources.length; i++) {
			if (!tempIds.includes(this.filteredResources[i].value.Id)) {
				this.selectedResources.push(this.filteredResources[i]);
			}
		}
		if (this.selectedResources.length > 0)
			this.noSelectedResources = false;
	}

	handleRemoveResource(event) {
		let resourceId = event.target.value;
		for (let i = 0; i < this.selectedResources.length; i++) {
			if (resourceId == this.selectedResources[i].value.Id) {
				this.selectedResources.splice(i, 1);
			}
		}
		if (this.selectedResources.length === 0)
			this.noSelectedResources = true;
	}

	handleTypeFilter(event) {
		this.listLoading = true;
		this.type = event.target.value;
	}

	handleDistanceFilter(event) {
		this.listLoading = true;
		this.distance = event.target.value;
	}

	handleMarkerSelect(event) {
		this.selectedMarkerValue = event.target.selectedMarkerValue;
	}

	handleHelp() {
		this.isHelpModalOpen = true;
	}

	closeHelpModal() {
		this.isHelpModalOpen = false;
	}

	handleFinish() {
		this.isModalOpen = true;
	}

	handleEmailAdd(event) {
		this.updateInput.fields.Email__c = event.target.value;
	}

	closeModal() {
		this.isModalOpen = false;
	}

	handleNext() {
		this.isModalOpen = false;
		this.screenLoading = true;
		for (let i = 0; i < this.selectedResources.length; i++) {
			this.recordInput.fields.Name__c = this.selectedResources[i].value.Name;
			this.recordInput.fields.Description__c = this.selectedResources[i].value.Description;
			this.recordInput.fields.Phone__c = this.selectedResources[i].value.Phone;
			this.recordInput.fields.Street_Address__c = this.selectedResources[i].value.BillingStreet;
			this.recordInput.fields.City__c = this.selectedResources[i].value.BillingCity;
			this.recordInput.fields.State__c = this.selectedResources[i].value.BillingState;
			this.recordInput.fields.Zip__c = this.selectedResources[i].value.BillingPostalCode;
			this.recordInput.fields.Food_Bank__c = this.selectedResources[i].value.Food_Bank__c;
			this.recordInput.fields.Financial_Assistance__c = this.selectedResources[i].value.Financial_Assistance__c;
			this.recordInput.fields.Shelter__c = this.selectedResources[i].value.Shelter__c;
			this.recordInput.fields.Case_Management__c = this.selectedResources[i].value.Case_Management__c;
			this.recordInput.fields.Serves_Meals__c = this.selectedResources[i].value.Serves_Meals__c;
			this.recordInput.fields.Senior_Center__c = this.selectedResources[i].value.Senior_Center__c;
			this.recordInput.fields.Housing_Assistance__c = this.selectedResources[i].value.Housing_Assistance__c;
			this.recordInput.fields.Transportation__c = this.selectedResources[i].value.Transportation__c;
			this.recordInput.fields.Social_Services_Assessment__c = this.recordId;
			this.recordInput.fields.Account__c = this.record[0].Account__r.Id;
			this.recordInput.fields.Resource_Account__c = this.selectedResources[i].value.Id;
			createRecord(this.recordInput)
			.then(() => {
				if (i == this.selectedResources.length - 1) {
					if (this.updateInput.fields.Email__c != '') {
						updateRecord(this.updateInput)
						.catch(error => {
							this.error = error;
							this.dispatchEvent(
								new ShowToastEvent({
									title: "Error",
									message: error.body.message,
									variant: "error"
								}),
							);
						});
						sendResources({email: this.updateInput.fields.Email__c, accountId: this.updateInput.fields.Id})
						.catch(error => {
							this.error = error;
							this.dispatchEvent(
								new ShowToastEvent({
									title: "Error",
									message: error.body.message,
									variant: "error"
								}),
							);
						});
					}
					searchActiveResources({aId: this.record[0].Account__r.Id})
					.then(results => {
						for (let item of results) {
							this.activeResources.push({value: item, phone: '(' + String(item.Phone__c).substring((String(item.Phone__c).length - 10), (String(item.Phone__c).length - 7)) + ')-' + String(item.Phone__c).substring((String(item.Phone__c).length - 7), (String(item.Phone__c).length - 4)) + '-' + String(item.Phone__c).substring((String(item.Phone__c).length - 4), String(item.Phone__c).length)});
						}
						this.screenLoading = false;
						this.vfpage = true;
					})
					.catch(() => {
						this.dispatchEvent(
							new ShowToastEvent({
								title: "Error",
								message: "There has been an error, please refresh.",
								variant: "error"
							}),
						);
					});
					this.selectedResources = [];
					this.noSelectedResources = true;
				}
			})
			.catch(error => {
				this.dispatchEvent(
					new ShowToastEvent({
						title: "Error",
						message: error.body.message,
						variant: "error"
					}),
				);
			});
		}
	}

	handleBack() {
		this.vfpage = false;
		this.activeResources = [];
	}

	handlePrint() {
		setTimeout(window.print(), 1000);
	}
}