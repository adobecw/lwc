import { LightningElement, track, api } from 'lwc';

export default class ResourceTile extends LightningElement {
    @api resource;
    @track types = [];
    handleAddClick() {
        const selectEvent = new CustomEvent('addresource', {
            detail: this.resource.value.Id
        });
        this.dispatchEvent(selectEvent);
    }

    handleShowMarker() {
        const showEvent = new CustomEvent('showmarker', {
            detail: this.resource.value.Id
        });
        this.dispatchEvent(showEvent);
    }

    // For badges
    /*connectedCallback() {
        if (this.resource.value.Food_Bank__c)
            this.types.push({ type: 'Food Bank', icon: 'custom:custom93' });
        if (this.resource.value.Serves_Meals__c)
            this.types.push({ type: 'Serves Meals', icon: 'custom:custom51' });
        if (this.resource.value.Shelter__c)
            this.types.push({ type: 'Shelter', icon: 'custom:custom61' });
        if (this.resource.value.Housing_Assistance__c)
            this.types.push({ type: 'Housing Assistance', icon: 'standard:home' });
        if (this.resource.value.Financial_Assistance__c)
            this.types.push({ type: 'Financial Assistance', icon: 'custom:custom41' });
        if (this.resource.value.Case_Management__c)
            this.types.push({ type: 'Case Management', icon: 'standard:case' });
        if (this.resource.value.Senior_Center__c)
            this.types.push({ type: 'Senior Center', icon: 'standard:partners' });
        if (this.resource.value.Transportation__c)
            this.types.push({ type: 'Transportation', icon: 'custom:custom31' });
    }*/

    // For pill container
    connectedCallback() {
        if (this.resource.value.Food_Bank__c)
            this.types.push({ type: 'icon', label: 'Food Bank', iconName: 'custom:custom93', alternativeText: 'Food Bank' });
        if (this.resource.value.Serves_Meals__c)
            this.types.push({ type: 'icon', label: 'Serves Meals', iconName: 'custom:custom51', alternativeText: 'Serves Meals' });
        if (this.resource.value.Shelter__c)
            this.types.push({ type: 'icon', label: 'Shelter', iconName: 'custom:custom61', alternativeText: 'Shelter' });
        if (this.resource.value.Housing_Assistance__c)
            this.types.push({ type: 'icon', label: 'Housing Assistance', iconName: 'standard:home', alternativeText: 'Housing Assistance' });
        if (this.resource.value.Financial_Assistance__c)
            this.types.push({ type: 'icon', label: 'Financial Assistance', iconName: 'custom:custom41', alternativeText: 'Financial Assistance' });
        if (this.resource.value.Case_Management__c)
            this.types.push({ type: 'icon', label: 'Case Management', iconName: 'standard:case', alternativeText: 'Case Management' });
        if (this.resource.value.Senior_Center__c)
            this.types.push({ type: 'icon', label: 'Senior Center', iconName: 'standard:partners', alternativeText: 'Senior Center' });
        if (this.resource.value.Transportation__c)
            this.types.push({ type: 'icon', label: 'Transportation', iconName: 'custom:custom31', alternativeText: 'Transportation' });
    }
}