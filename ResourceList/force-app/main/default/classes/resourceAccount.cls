public with sharing class resourceAccount {
    @AuraEnabled(cacheable=true)
    public static Account checkAccounts(String fn, String ln, Date bd) {
         return [
            SELECT Id
            FROM Account
            WHERE First_Name__c = :fn AND Surname__c = :ln AND Birthdate__c = :bd
            LIMIT 1
        ];
    }
}