public with sharing class resourceList {
    @AuraEnabled(cacheable=true)
    public static List<Social_Services_Assesment__c> searchRecord(Id id) {
        return [SELECT Account__r.Id, Account__r.BillingStreet, Account__r.BillingCity, Account__r.BillingState, Account__r.Latitude__c, Account__r.Longitude__c, What_type_of_residence_do_you_live_in__c, Are_you_having_financial_difficulties__c, Can_you_pay_for_utilities__c, Can_you_pay_for_housing__c, Can_you_pay_for_your_medical_visits__c, Can_you_pay_for_your_transportation__c, Do_you_eat_at_least_two_meals_a_day__c, Can_you_pay_for_food__c, Can_you_pay_for_your_medication__c, How_do_you_get_to_medical_appointments__c, Do_you_feel_safe_at_home__c, Police_called_to_home_in_last_90_days__c
                FROM Social_Services_Assesment__c
                WHERE Id = :id
                LIMIT 1];
    }

    @AuraEnabled(cacheable=true)
    public static List<Selected_Resource__c> searchActiveResources(Id aId) {
        return [SELECT Id, Name__c, Description__c, Phone__c, Street_Address__c, City__c, State__c, Zip__c
                FROM Selected_Resource__c
                WHERE Account__c = :aId AND Active__c = True 
                ORDER BY CreatedDate Desc];
    }

    @AuraEnabled(cacheable=true)
    public static Boolean sendResources(String email, Id accountId) {
        List<Selected_Resource__c> resources = new List<Selected_Resource__c>([SELECT Name, Name__c, Description__c, Phone__c, Street_Address__c, City__c, State__c, Zip__c 
                                                                               FROM Selected_Resource__c 
                                                                               WHERE Account__c = :accountId AND Active__c = True
                                                                               ORDER BY CreatedDate Desc 
                                                                               LIMIT 20]);
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.setToAddresses(new String[] {email});
        message.setReplyTo('noreply@maslow.com');
        message.setSenderDisplayName('Maslow');
        message.setSubject('Selected Resources');
        String body = '';
        for (Integer i = 0; i < resources.size(); i++) {
            body = body + '<h2>' + resources[i].Name__c + '</h2><p>' + 'Description: ' + resources[i].Description__c + '</p><p>' + 'Phone: (' + resources[i].Phone__c.substring(0, 3) + ')-' + resources[i].Phone__c.substring(3, 6) + '-' + resources[i].Phone__c.substring(6, 10) + '</p><p>' + 'Address: ' + resources[i].Street_Address__c + ', ' + resources[i].City__c + ', ' + resources[i].State__c + ' ' + resources[i].Zip__c +'</p><br></br>';
        }
        message.setHtmlBody(body);
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        System.debug('Sent');
        if (results[0].isSuccess())
            return true;
        else
            return false;
    }

    @AuraEnabled(cacheable=true)
    public static Map<Decimal,Account> searchResources(String state, String type, Integer distance, Double latitude, Double longitude, String What_type_of_residence_do_you_live_in, String Are_you_having_financial_difficulties, String Can_you_pay_for_utilities, String Can_you_pay_for_housing, String Can_you_pay_for_your_medical_visits, String Can_you_pay_for_your_transportation, String Do_you_eat_at_least_two_meals_a_day, String Can_you_pay_for_food, String Can_you_pay_for_your_medication, String How_do_you_get_to_medical_appointments, String Do_you_feel_safe_at_home, String Police_called_to_home_in_last_90_days) {
        // Initialize distance variables
        Double lat1 = latitude;
        Double lon1 = longitude;
        Double lat2;
        Double lon2;
        Double x;
        Double y;
        Double xx;
        Double yy;
        Double a;
        Double c;
        Decimal d;

        // Get all resources and create lists and maps for data manipulation
        List<Account> InitialResources = [SELECT Id, Name, Phone, Description, BillingStreet, BillingCity, BillingState, BillingPostalCode, Latitude__c, Longitude__c, Food_Bank__c, Financial_Assistance__c, Shelter__c, Case_Management__c, Serves_Meals__c, Senior_Center__c, Housing_Assistance__c, Transportation__c
                                          FROM Account
                                          WHERE RecordTypeId = '0121R000000yZGLQA2' AND BillingState = :state];
        List<Decimal> Distances = new List<Decimal>();
        Map<Decimal,Id> DirtyMap = new Map<Decimal,Id>();
        List<Id> Ids = new List<Id>();
        List<Id> InitialIds = new List<Id>();
        List<Account> SortedResources = new List<Account>();
        Map<Decimal,Account> FilteredMap = new Map<Decimal,Account>();

        // Get distance in miles of each resource and add to Distance__c
        for (Account record : InitialResources){
            lat2 = record.Latitude__c;
            lon2 = record.Longitude__c;
            x = lat1 * 3.141592 / 180;
            y = lat2 * 3.141592 / 180;
            xx = (lat2 - lat1) * 3.141592 / 180;
            yy = (lon2 - lon1) * 3.141592 / 180;
            a = math.sin(xx/2) * math.sin(xx/2) + math.cos(x) * math.cos(y) * math.sin(yy/2) * math.sin(yy/2);
            c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a));
            d = 6371000 * c;
            d = d * 0.00062137;
            Distances.add(d);
        }

        // Sort InitialResources by Distance__c
        for (Integer i = 0; i < InitialResources.size(); i++){
            if (Distances[i] <= distance)
                DirtyMap.put(Distances[i], InitialResources[i].Id);
        }
        Distances.sort();
        for (Integer i = 0; i < Distances.size(); i++){
            Ids.add(DirtyMap.get(Distances[i]));
        }
        // This part is very inefficient, there has to be a better way of doing this
        for (Integer i = 0; i < InitialResources.size(); i++){
            InitialIds.add(InitialResources[i].Id);
        } 
        for (Integer i = 0; i < Ids.size(); i++){
            if (InitialIds.contains(Ids[i]))
                for (Integer j = 0; j < InitialResources.size(); j++){
                    if (InitialResources[j].Id == Ids[i])
                        SortedResources.add(InitialResources[j]);
                }
        }

        // Return resources depending on filters
        if (type == 'Recommended Resources') {
            for (Integer i = 0; i < SortedResources.size(); i++){
                if (SortedResources[i].Food_Bank__c && Can_you_pay_for_food == 'No')
                    FilteredMap.put(Distances[i], SortedResources[i]);
                else if (SortedResources[i].Financial_Assistance__c && (Are_you_having_financial_difficulties == 'Yes' || Can_you_pay_for_utilities == 'No' || Can_you_pay_for_housing == 'No' || Can_you_pay_for_your_medical_visits == 'No' || Can_you_pay_for_your_transportation == 'No' || Can_you_pay_for_your_medication == 'No'))
                    FilteredMap.put(Distances[i], SortedResources[i]);
                else if (SortedResources[i].Shelter__c && What_type_of_residence_do_you_live_in == 'Homeless')
                    FilteredMap.put(Distances[i], SortedResources[i]);
                else if (SortedResources[i].Serves_Meals__c && Do_you_eat_at_least_two_meals_a_day == 'No')
                    FilteredMap.put(Distances[i], SortedResources[i]);
                else if (SortedResources[i].Housing_Assistance__c && Can_you_pay_for_housing == 'No')
                    FilteredMap.put(Distances[i], SortedResources[i]);
                else if (SortedResources[i].Transportation__c && How_do_you_get_to_medical_appointments == 'No fixed arrangement')
                    FilteredMap.put(Distances[i], SortedResources[i]);
            }
            return FilteredMap;
        }
        else if (type == 'Food Bank') {
            for (Integer i = 0; i < SortedResources.size(); i++){
                if (SortedResources[i].Food_Bank__c)
                    FilteredMap.put(Distances[i], SortedResources[i]);
            }
            return FilteredMap;
        }
        else if (type == 'Financial Assistance'){
            for (Integer i = 0; i < SortedResources.size(); i++){
                if (SortedResources[i].Financial_Assistance__c)
                    FilteredMap.put(Distances[i], SortedResources[i]);
            }
            return FilteredMap;
        }
        else if (type == 'Shelter'){
            for (Integer i = 0; i < SortedResources.size(); i++){
                if (SortedResources[i].Shelter__c)
                    FilteredMap.put(Distances[i], SortedResources[i]);
            }
            return FilteredMap;
        }
        else if (type == 'Case Management'){
            for (Integer i = 0; i < SortedResources.size(); i++){
                if (SortedResources[i].Case_Management__c)
                    FilteredMap.put(Distances[i], SortedResources[i]);
            }
            return FilteredMap;
        }
        else if (type == 'Serves Meals'){
            for (Integer i = 0; i < SortedResources.size(); i++){
                if (SortedResources[i].Serves_Meals__c)
                    FilteredMap.put(Distances[i], SortedResources[i]);
            }
            return FilteredMap;
        }
        else if (type == 'Senior Center'){
            for (Integer i = 0; i < SortedResources.size(); i++){
                if (SortedResources[i].Senior_Center__c)
                    FilteredMap.put(Distances[i], SortedResources[i]);
            }
            return FilteredMap;
        }
        else if (type == 'Housing Assistance'){
            for (Integer i = 0; i < SortedResources.size(); i++){
                if (SortedResources[i].Housing_Assistance__c)
                    FilteredMap.put(Distances[i], SortedResources[i]);
            }
            return FilteredMap;
        }
        else if (type == 'Transportation'){
            for (Integer i = 0; i < SortedResources.size(); i++){
                if (SortedResources[i].Transportation__c)
                    FilteredMap.put(Distances[i], SortedResources[i]);
            }
            return FilteredMap;
        }
        else {
            for (Integer i = 0; i < SortedResources.size(); i++){
                FilteredMap.put(Distances[i], SortedResources[i]);
            }
            return FilteredMap;
        }
    }
}